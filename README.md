# Vltava Rozhlas PyDL
This program is intended to download podcasts in Czech.
GUI variant of the program has Czech labels, because I created it
for someone who can't speak English.

Vltava Rozhlas PyDL is a Python program for downloading from https://vltava.rozhlas.cz  
**Please note that this is just an command line wrapper and GUI interface to
[youtube-dl](http://ytdl-org.github.io/youtube-dl/) to download from Vltava Rozhlas.  
You need to have youtube-dl (and optionally [ffmpeg](https://ffmpeg.org/)) installed in your system
or have the youtube-dl binary accessible from `$PATH` environment variable.**  
**This project doesn't (and never will) supply youtube-dl, nor ffmpeg binaries.**

## Prerequisites
You must have youtube-dl, and optionally ffmpeg installed in your system, available to use from `$PATH` environment variable.  
On Windows, you can have `youtube-dl.exe` and/or `ffmpeg.exe` stored in the directory where you cloned this repo.

On Linux, the most straightforward way to install youtube-dl and ffmpeg is by a package manager.  
For example, you can use command `dnf install youtube-dl ffmpeg` on Fedora Linux.

On Windows, you can download youtube-dl and ffmpeg binaries here:
* http://ytdl-org.github.io/youtube-dl/download.html
* https://ffmpeg.org/download.html

Or you can install it by winget, but I don't recommend that because winget offers several different
packages and I don't know which one is correct.
```shell
PS C:\Users\predseda> winget search youtube-dl
Name                  Id                           Version    Match               Source
----------------------------------------------------------------------------------------
youtube-dl            youtube-dl.youtube-dl        2021.12.17                     winget
yt-dlp                yt-dlp.yt-dlp                2023.03.04 Tag: youtube-dl     winget
WebDL                 Timber1900.WebDL             v11.3.1    Tag: youtube-dl     winget
Open Video Downloader jely2002.youtube-dl-gui      2.4.0      Tag: youtube-dl     winget
Videomass             GianlucaPernigotto.Videomass 3.5.8      Tag: youtube-dl-gui winget
PS C:\Users\predseda> winget search ffmpeg
Name                      Id                           Version Match           Source
-------------------------------------------------------------------------------------
FFmpeg                    Gyan.FFmpeg                  6.0                     winget
Wav2Bar                   Picorims.wav2bar             0.3.0   Tag: ffmpeg     winget
y2mp3                     moshfeu.y2mp3                2.5.7   Tag: ffmpeg     winget
FFmpeg (Shared)           Gyan.FFmpeg.Shared           5.1.2   Tag: ffmpeg     winget
FFmpeg Batch AV Converter eibol.FFmpegBatchAVConverter 2.8.1                   winget
Videomass                 GianlucaPernigotto.Videomass 3.5.8   Tag: ffmpeg-gui winget
```
The easiest way on Windows is probably to download youtube-dl and ffmpeg binaries from the official websites
and store binaries directly to the cloned repo directory.

### Supported OS
Only Linux and Windows NT (10 and later) systems are supported.  
But it should probably work on most Unix-like systems where youtube-dl, Python, and Qt are available.  
If shell variant is enough for you, you need just youtube-dl and Python.

## Linux cli variant
```shell
# Create venv
python3 -m venv venv
source venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt

# Run cli.py
python3 cli.py -u <url> -d <download-dir>

# Deactivate venv
deactivate
```

## Windows NT cli variant
```shell
# Create venv
python -m venv venv
.\venv\Scripts\Activate.ps1
python -m pip install --upgrade pip
python -m pip install -r requirements.txt

python cli.py -u <url> -d <download-dir>

# Deactivate venv
deactivate
```

## GUI variant
To use GUI variant, you have two options:
* Run `gui.py` from shell
* Build a binary using [pyinstaller](https://pyinstaller.org/)

### Run gui.py from shell
Steps are very same as for cli variants, but run script `gui.py` instead of `cli.py`.
#### Linux
```
# Create venv
python3 -m venv venv
source venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt

# Run gui.py
python3 gui.py

# Deactivate venv
deactivate
```

#### Windows NT
```powershell
# Create venv
python -m venv venv
.\venv\Scripts\Activate.ps1
python -m pip install --upgrade pip
python -m pip install -r requirements.txt

# Run gui.py
python gui.py

# Deactivate venv
deactivate
```

### Build a binary using pyinstaller
(in venv):
```shell
# Install pyinstaller
python3 -m pip install pyinstaller

# Build a binary
pyinstaller --onefile --windowed gui.py
```

The built binary should be in directory `dist`. You can execute it from a file manager by mouse click.
