import sys
import os

from PySide6 import QtCore, QtWidgets, QtGui
from PySide6 import __version__ as QtVersion

from rozhlas import get_playlist, find_youtube_dl

VERSION = '0.2.4'


class MainWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.urlLabel = QtWidgets.QLabel('Odkaz na Vltava Rozhlas:')
        self.url = QtWidgets.QLineEdit()
        self.url.setPlaceholderText('Vltava Rozhlas URL')

        self.targetDirLabel = QtWidgets.QLabel('Adresář pro uložení:')
        self.targetDir = QtWidgets.QLineEdit()
        self.targetDir.setPlaceholderText('Adresář pro uložení stažených souborů')

        self.fileDialogButton = QtWidgets.QPushButton('Vybrat umístění')

        self.downloadButton = QtWidgets.QPushButton('Stáhnout')

        self.layout = QtWidgets.QGridLayout(self)
        self.layout.addWidget(self.urlLabel, 0, 0)
        self.layout.addWidget(self.url, 0, 1)
        self.layout.addWidget(self.targetDirLabel, 1, 0)
        self.layout.addWidget(self.targetDir, 1, 1)
        self.layout.addWidget(self.fileDialogButton, 1, 2)
        self.layout.addWidget(self.downloadButton, 2, 1)

        self.downloadButton.clicked.connect(self.downloadWindow)
        self.fileDialogButton.clicked.connect(self.selectDownloadDir)

        self.foundSupportedFormat = False
        self.procFinished = False

    @QtCore.Slot()
    def downloadWindow(self):
        url = self.url.text()
        if not url:
            self.emptyUrl()
            return

        downloadDir = self.targetDir.text()
        if not downloadDir:
            self.emptyDownloadDir()
            return

        self.logWindow = QtWidgets.QDialog()
        self.logWindow.setWindowTitle('Průběh stahování')
        self.logWindow.setModal(True)
        self.logWindow.resize(450, 450)
        self.logWindow.setWindowFlag(QtCore.Qt.WindowCloseButtonHint, False)

        self.log = QtWidgets.QPlainTextEdit(parent=self.logWindow)
        self.log.setReadOnly(True)
        self.log.centerOnScroll()

        self.interruptDownloadButton = QtWidgets.QPushButton('Přerušit stahování')
        self.interruptDownloadButton.clicked.connect(self.interruptDownload)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.log)
        layout.addWidget(self.interruptDownloadButton)

        self.logWindow.setLayout(layout)

        self.logWindow.open()

        downloadSuccess = self.download(url, downloadDir)
        if not downloadSuccess:
            return

        self.log.appendPlainText('Stahování dokončeno')
        # Enable close button
        self.logWindow.setWindowFlag(QtCore.Qt.WindowCloseButtonHint)
        self.logWindow.show()

    def download(self, url, downloadDir):
        """
        Downloads a play from playlist.

        :param url: URL to download
        :param downloadDir: Directory to store downloaded file
        :returns: True on successful download, False failure
        """

        playlist = get_playlist(url)
        if not playlist:
            self.playlistNotFound()
            self.logWindow.close()
            return False

        if not os.path.isdir(downloadDir):
            os.mkdir(downloadDir)

        self.downloadInterrupted = False
        part = 1
        for play in playlist:
            for audiolink in play['audioLinks']:
                link = audiolink['url']
                if '.mp3' in link:
                    self.format = 'mp3'
                    self.foundSupportedFormat = True
                elif '.mp4' in link:
                    self.format = 'mp4'
                    self.foundSupportedFormat = True
                elif '.mpd' in link:
                    self.format = 'm4a'
                    self.foundSupportedFormat = True
                else:
                    if audiolink == play['audioLinks'][-1] and not self.foundSupportedFormat:
                        self.formatUnsupportedWindow()
                        self.logWindow.close()
                        return
                    continue

                if part < 10:
                    filename = '0' + str(part)
                else:
                    filename = str(part)
                output = os.path.join(downloadDir, f'{filename}.{self.format}')

                self.proc = QtCore.QProcess(self)
                self.proc.setProgram('youtube-dl')
                self.proc.setArguments(['--newline', '-o', output, link])
                self.proc.readyReadStandardOutput.connect(self.on_readyReadStandardOutput)
                self.proc.readyReadStandardError.connect(self.on_readyReadStandardError)
                self.proc.started.connect(self.downloadProcessStarted)
                self.proc.finished.connect(self.downloadProcessFinished)
                self.proc.start()

                self.proc.waitForStarted()

                # Wait in a new event loop for youtube-dl process to finish
                loop = QtCore.QEventLoop()
                self.proc.finished.connect(loop.quit)
                loop.exec()
                if loop.isRunning():
                    loop.quit()

                part += 1

                if self.downloadInterrupted:
                    return

                # We want to download only one format for each part
                break

        return True

    @QtCore.Slot()
    def on_readyReadStandardOutput(self):
        out = self.proc.readAllStandardOutput().data().decode().strip()
        self.log.appendPlainText(out)

    @QtCore.Slot()
    def on_readyReadStandardError(self):
        err = self.proc.readAllStandardError().data().decode().strip()
        self.log.appendPlainText(err)

    @QtCore.Slot()
    def interruptDownload(self):
        if not self.downloadInterrupted:
            self.proc.kill()

            self.downloadInterrupted = True

            self.log.appendPlainText('Stahování přerušeno')

            # Enable close button
            self.logWindow.setWindowFlag(QtCore.Qt.WindowCloseButtonHint)
            self.logWindow.show()

    @QtCore.Slot()
    def downloadProcessFinished(self):
        self.procFinished = True
        self.interruptDownloadButton.setEnabled(False)

    @QtCore.Slot()
    def downloadProcessStarted(self):
        self.interruptDownloadButton.setEnabled(True)

    # https://doc.qt.io/qtforpython/PySide6/QtWidgets/QFileDialog.html
    @QtCore.Slot()
    def selectDownloadDir(self):
        downloadDir = QtWidgets.QFileDialog.getExistingDirectory(
            self, "Vyberte složku pro stažení souborů", QtCore.QDir.homePath(), QtWidgets.QFileDialog.ShowDirsOnly
        )
        self.targetDir.setText(downloadDir)

    @QtCore.Slot()
    def emptyUrl(self):
        errorWindow = QtWidgets.QDialog()

        errorWindow.setWindowTitle("Chyba")
        errorWindow.setModal(True)

        button = QtWidgets.QDialogButtonBox.StandardButton.Ok
        buttonBox = QtWidgets.QDialogButtonBox(button)
        buttonBox.accepted.connect(errorWindow.accept)

        layout = QtWidgets.QVBoxLayout()
        message = QtWidgets.QLabel('Chyba: Nezadané URL')
        layout.addWidget(message)
        layout.addWidget(buttonBox)
        errorWindow.setLayout(layout)

        errorWindow.exec()

    @QtCore.Slot()
    def emptyDownloadDir(self):
        errorWindow = QtWidgets.QDialog()

        errorWindow.setWindowTitle("Chyba")
        errorWindow.setModal(True)

        button = QtWidgets.QDialogButtonBox.StandardButton.Ok
        buttonBox = QtWidgets.QDialogButtonBox(button)
        buttonBox.accepted.connect(errorWindow.accept)

        layout = QtWidgets.QVBoxLayout()
        message = QtWidgets.QLabel('Chyba: Nezadán adresář pro uložení stažených souborů')
        layout.addWidget(message)
        layout.addWidget(buttonBox)
        errorWindow.setLayout(layout)

        errorWindow.exec()

    def formatUnsupportedWindow(self):
        errorWindow = QtWidgets.QDialog()

        errorWindow.setWindowTitle("Chyba")
        errorWindow.setModal(True)

        button = QtWidgets.QDialogButtonBox.StandardButton.Ok
        buttonBox = QtWidgets.QDialogButtonBox(button)
        buttonBox.accepted.connect(errorWindow.accept)

        layout = QtWidgets.QVBoxLayout()
        message = QtWidgets.QLabel('Formát není podporován, nelze stáhnout :-(')
        layout.addWidget(message)
        layout.addWidget(buttonBox)
        errorWindow.setLayout(layout)

        errorWindow.exec()

    def playlistNotFound(self):
        errorWindow = QtWidgets.QDialog()

        errorWindow.setWindowTitle("Chyba")
        errorWindow.setModal(True)

        button = QtWidgets.QDialogButtonBox.StandardButton.Ok
        buttonBox = QtWidgets.QDialogButtonBox(button)
        buttonBox.accepted.connect(errorWindow.accept)

        layout = QtWidgets.QVBoxLayout()
        message = QtWidgets.QLabel(
            'Playlist nenalezen, asi se změnila struktura webu :-(\n' \
            'Je odkaz správně zadaný?'
        )
        layout.addWidget(message)
        layout.addWidget(buttonBox)
        errorWindow.setLayout(layout)

        errorWindow.exec()


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.resize(800, 600)

        self.aboutMenu = self.menuBar().addMenu('O programu')
        self.aboutAction = QtGui.QAction('Informace', self)
        self.aboutAction.triggered.connect(self.about)
        self.aboutMenu.addAction(self.aboutAction)

        self.mainWidget = MainWidget()
        self.setCentralWidget(self.mainWidget)

        self.haveYoutubeDl = True
        self.youtubedlNotFound()
        if not self.haveYoutubeDl:
            sys.exit(1)

    def about(self):
        text = f'''Verze: {VERSION}<br>
        Verze Qt: {QtVersion}<br>
        Zdrojový kód je dostupný na GitLabu:
        <a href="https://gitlab.com/predseda/vltava-rozhlas-pydl">https://gitlab.com/predseda/vltava-rozhlas-pydl</a>
        '''
        info = QtWidgets.QTextEdit(text)

        aboutWindow = QtWidgets.QDialog()
        aboutWindow.setWindowTitle('O programu')
        aboutWindow.resize(600, 400)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(info)

        aboutWindow.setLayout(layout)

        aboutWindow.exec()


    def youtubedlNotFound(self):
        if not find_youtube_dl():
            self.haveYoutubeDl = False
            errorWindow = QtWidgets.QDialog()

            errorWindow.setWindowTitle("Chyba")
            errorWindow.setModal(True)

            button = QtWidgets.QDialogButtonBox.StandardButton.Ok
            buttonBox = QtWidgets.QDialogButtonBox(button)
            buttonBox.accepted.connect(errorWindow.accept)

            layout = QtWidgets.QVBoxLayout()
            message = QtWidgets.QLabel('Podpůrný program youtube-dl nebyl nalezen!\nBez něj není možné stahovat z Vltava Rozhlasu.')
            layout.addWidget(message)
            layout.addWidget(buttonBox)
            errorWindow.setLayout(layout)

            errorWindow.exec()


if __name__ == '__main__':
    app = QtWidgets.QApplication([])

    mainWindow = MainWindow()
    mainWindow.show()

    sys.exit(app.exec())
