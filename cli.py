#!/usr/bin/env python3

import os
import sys
import subprocess
from argparse import ArgumentParser

from rozhlas import get_playlist, find_youtube_dl


def parseArgs():
    parser = ArgumentParser()

    parser.add_argument('-u', '--url', required=True, help='URL Vltava Rozhlas to download from')
    parser.add_argument('-d', '--download_dir', required=True, help='Directory to store downloaded files')

    return parser.parse_args()


def download(url, download_dir):
    playlist = get_playlist(url)
    if not playlist:
        print(
            'Failed to get playlist, perhaps the website structure has changed :-(\n' \
            'Is the url correct?',
            file=sys.stderr
        )
        sys.exit(1)

    if not os.path.isdir(download_dir):
        os.mkdir(download_dir)

    part = 1
    for play in playlist:
        foundSupportedFormat = False
        for audiolink in play['audioLinks']:
            link = audiolink['url']
            if '.mp3' in link:
                format = 'mp3'
                foundSupportedFormat = True
            elif '.mp4' in link:
                format = 'mp4'
                foundSupportedFormat = True
            elif '.mpd' in audiolink['url']:
                format = 'm4a'
                foundSupportedFormat = True
            else:
                # Skip part if the last link in the playlist is in unsupported format
                if audiolink == play['audioLinks'][-1] and not foundSupportedFormat:
                    print(
                        f'ERROR: not supported format for part {part}',
                        file=sys.stderr
                    )
                    part += 1
                continue

            if part < 10:
                filename = '0' + str(part)
            else:
                filename = str(part)

            output = os.path.join(download_dir, f'{filename}.{format}')
            subprocess.run(['youtube-dl', '-o', output, link], shell=True)
            part += 1

            # We want to download only one format for each part
            break


if __name__ == '__main__':
    args = parseArgs()

    with open('version.txt', 'r') as f:
        version = f.readline().strip()
        print(f'Version: {version}')

    url = args.url
    downloadDir = args.download_dir

    if not find_youtube_dl():
        sys.exit(1)

    download(url, downloadDir)
