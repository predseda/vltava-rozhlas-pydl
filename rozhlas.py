import sys
import requests
import json
from bs4 import BeautifulSoup as bs
from shutil import which


def find_youtube_dl():
    """
    Returns True if youtube-dl was found, False otherwise.
    """

    youtube_dl = which('youtube-dl')
    if not youtube_dl:
        print('ERROR: youtube-dl not found', file=sys.stderr)
        return False

    return True


def fetch_page(url):
    """
    :param url: URL (vltava.rozhlas.cz) to download from
    :returns: Downloaded web page on success, None on failure
    """

    try:
        page = requests.get(url)
    except requests.exceptions.MissingSchema:
        return None
    if page.status_code != requests.codes.ok:
        print(f'Failed to fetch download url: HTTP {page.status_code}', file=sys.stderr)
        return None

    return page


def get_playlist(url):
    """
    Extracts playlist from a Vltava Rozhlas URL.

    :param url: Vltava Rozhlas URL
    :returns: Playlist (JSON) if found in the website, None otherwise
    """

    page = fetch_page(url)
    if not page:
        return None

    soup = bs(page.content, 'html.parser')
    results = soup.find('div', class_='mujRozhlasPlayer')
    if not results:
        print('Could not find data-player div', file=sys.stderr)
        return None

    try:
        data_player = json.loads(results['data-player'])
        playlist = data_player['data']['playlist']
    except IndexError:
        print(
            'ERROR: Could not find playlist, perhaps the website structure has changed?',
            file=sys.stderr
        )
        return None

    return playlist
